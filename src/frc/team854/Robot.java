/*
 * Class: Robot
 * Author: Robert Ciborowski, Waseef Nayeem and Julian Dominguez-Schatz
 * Date: 06/01/2018
 * Description: A class which represents the robot as a whole. Consider it
 *              to be the main class.
 *              
 *              The VM is configured to automatically run this class, and to call the
 *              functions corresponding to each mode, as described in the IterativeRobot
 *              documentation. If you change the name of this class or the package after
 *              creating this project, you must also update the manifest file in the resource
 *              directory.
 */

package frc.team854;

import edu.wpi.first.wpilibj.*;
import edu.wpi.first.wpilibj.command.Scheduler;
import frc.team854.hardware.DeviceProvider;
import frc.team854.operatorinterface.OperatorInterface;
import frc.team854.recording.Recorder;
import frc.team854.subsystems.ChassisSubsystem;

public class Robot extends frc.team854.CustomIterativeRobot {
	// These are the subsystems of the robot.
	public static ChassisSubsystem chassisSubsystem;

	// This provides all devices to the robot.
	public static DeviceProvider devices;

	// These represent other parts of the robot.
	public static OperatorInterface oi;
	public static PowerDistributionPanel pdp;
	
	public static RobotMode currentMode;

	private void initDevices() {
		devices = new DeviceProvider();
		
		// This is where device initialisation begins.
		// An example of device initialisation is:
		// devices.putDevice(InterfaceType.PWM, RobotInterfaceConstants.PORT_MOTOR_ARM,
		//		new Spark(RobotInterfaceConstants.PORT_MOTOR_ARM));

		// These are the analog devices.
		// <insert devices here>

		// These are the digital devices.
		//

		// These are the PWM devices.
		//


		// These are the PCM devices.
		//
		
		// This is our MXP device, an ADIS16448 gyro. It automatically takes the next 4 seconds to calibrate,
		// blocking changes in other WPILib devices.
		// ADIS16448_IMU gyro = new ADIS16448_IMU();
		// devices.putDevice(InterfaceType.MXP, RobotInterfaceConstants.PORT_GYRO, gyro);
	}

	/**
	 * The robot's initialisation method.
	 */
	public void robotInit() {
		chassisSubsystem = new ChassisSubsystem();
		chassisSubsystem.setEnabled(true);

		oi = new OperatorInterface();
		pdp = new PowerDistributionPanel();

		CameraServer.getInstance().startAutomaticCapture("Front camera", 0);

		initDevices();
		initDashboard();
		updateDashboard();
	}
	
	private void initDashboard() {
		// This is a useless function
	}
	
	@Override
	public void robotPeriodic() {
		// This is a useless function.
	}

	/** This runs when the robot's disabled mode is enabled.*/
	public void disabledInit() {
		currentMode = RobotMode.DISABLED;
		chassisSubsystem.setCurrentMode(RobotMode.DISABLED);
		updateDashboard();
	}

	/** This runs during the robot's disabled mode, periodically.*/
	public void disabledPeriodic() {
		Scheduler.getInstance().run();
		updateDashboard();
	}

	/**
	 * This runs when the robot's autonomous mode is enabled.
	 */
	public void autonomousInit() {
		currentMode = RobotMode.AUTONOMOUS;
		chassisSubsystem.setCurrentMode(RobotMode.AUTONOMOUS);
		oi.setCurrentMode(RobotMode.AUTONOMOUS);

        updateDashboard();
    }

	/**
	 * This runs during the robot's autonomous mode, periodically.
	 */
	public void autonomousPeriodic() {
		Scheduler.getInstance().run();
		subsystemPeriodic();
		updateDashboard();
	}

	/**
	 * This runs when the robot's disabled mode is enabled.
	 */
	public void teleopInit() {
		currentMode = RobotMode.TELEOPERATED;
		chassisSubsystem.setCurrentMode(RobotMode.TELEOPERATED);

		updateDashboard();
	}

	/** This runs during the robot's tele-operated mode, periodically.*/
	public void teleopPeriodic() {
		Scheduler.getInstance().run();
		subsystemPeriodic();
		updateDashboard();
	}
	
	/** This runs when the robot's test mode is enabled.*/
	public void testInit() {
		currentMode = RobotMode.TEST;
		chassisSubsystem.setCurrentMode(RobotMode.TEST);
		oi.setCurrentMode(RobotMode.TEST);
		updateDashboard();
	}

	/** This runs during the robot's disabled mode, periodically.*/
	public void testPeriodic() {}

	/**
	 * This runs every subsystem's periodic method.
	 */
	private void subsystemPeriodic() {
		chassisSubsystem.periodic();
	}

	/** This updates the FRC dashboard.*/
	private void updateDashboard() {
		// This updates all subsystem OI dashboard items.
		chassisSubsystem.updateDashboard();
		oi.updateDashboard();
	}
}
