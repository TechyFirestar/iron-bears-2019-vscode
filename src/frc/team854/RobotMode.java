package frc.team854;

public enum RobotMode {
	DISABLED, AUTONOMOUS, TELEOPERATED, TEST
}
