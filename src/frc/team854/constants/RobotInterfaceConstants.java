/*
 * Class: RobotInterfaceConstants
 * Author: Everyone
 * Date: 13/01/2018
 * Description: An interface which stores constants related to the inputs and outputs on
 *              the RoboRIO.
 */

package frc.team854.constants;

public interface RobotInterfaceConstants {
	// An example of a port constant: public static final int PORT_PNEUMATIC_LEFT = 7;

}
